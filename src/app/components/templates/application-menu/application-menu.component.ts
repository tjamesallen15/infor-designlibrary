import { Component, OnChanges, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { SohoEngineComponent } from '../../soho-engine/soho-engine.component';
import { HeaderComponent } from '../../templates/header/header.component';
import { IconViewComponent } from '../../icons/icon-view/icon-view.component';

@Component({
  selector: 'app-application-menu',
  templateUrl: './application-menu.component.html',
  styleUrls: ['./application-menu.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationMenuComponent implements OnChanges, OnInit, ChangeDetectorRef {
  /* Public properties */
  public types: any[] = [];
  public currentTab: String = '';

  /* Private fields */
  private sohoEngine: SohoEngineComponent = null;

  constructor(private ref: ChangeDetectorRef, private _http: Http, private router: Router) {
    const self = this;
    
    this.sohoEngine = new SohoEngineComponent(_http);

    this.sohoEngine.afterGetTypes = function(types) {
      self.types = types;

      self.ref.detectChanges();
    };
  }

  /* Implemented functions */
  checkNoChanges() { }
  detach() { }
  detectChanges() { }
  markForCheck() { }
  ngOnInit() { }
  ngOnChanges() { }
  reattach() { }

  /* Public functions */
  public capitalizeFirstLetter(str) {
    return this.sohoEngine.capitalizeFirstLetter(str);
  }

  public filterByType(type) {
    IconViewComponent.isSearch = false;
    this.sohoEngine.filterIcons(type, 'type');
  }

  public setMainTitle(title, key) {
    HeaderComponent.setMainTitle(title);
    HeaderComponent.changeAccessPage(true);
    this.collapseTabs(title);
    let urlRedirect = 'hnl-dev/2.1.1/app-' + key + '-view';
    if (this.getAccessLevel()) {
      urlRedirect = 'hnl-dev/2.1.1/app-' + key + '-view' + '?al=' + this.getAccessLevel();
    }
    this.router.navigateByUrl(urlRedirect);
  }

  public getAccessLevel() {
    const url_string = this.getRootLink();
    const url = new URL(url_string);
    const accessLevel = url.searchParams.get('al');

    return accessLevel;
  }

  public getRootLink() {
    return window.location.href;
  }


  public collapseTabs(title) {
    if (this.currentTab === '') {
      this.currentTab = title;
    }

    if (this.currentTab !== title) {
      this.currentTab = title;
      const elem = $('#hnl-accordion');
      const api = elem.data('accordion');
      api.collapseAll();
    }

  }
}
