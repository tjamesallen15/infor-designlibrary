import { Component, OnChanges, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { SohoEngineComponent } from '../../soho-engine/soho-engine.component';
import { IconViewComponent } from '../../icons/icon-view/icon-view.component';

@Component({
  selector: 'app-drop-panel',
  templateUrl: './drop-panel.component.html',
  styleUrls: ['./drop-panel.component.css']
})
export class DropPanelComponent implements OnChanges, OnInit, ChangeDetectorRef {
  /* Static Fields */
  static dropPanelLabel: any = 'Show';
  get dropPanelLabel(): any { return DropPanelComponent.dropPanelLabel; }
  set dropPanelLabel(val: any) { DropPanelComponent.dropPanelLabel = val; }
  static sohoEngine: SohoEngineComponent = null;
  get sohoEngine(): any { return DropPanelComponent.sohoEngine; }
  set sohoEngine(val: any) { DropPanelComponent.sohoEngine = val; }
  static selectedIcons: any[] = [];
  get selectedIcons(): any { return DropPanelComponent.selectedIcons; }
  set selectedIcons(val: any) { DropPanelComponent.selectedIcons = val; }

  /* Public properties */
  public clearAll: any = null;
  public getIconSrc: any = null;
  public isNewProduct: any = false;
  public prodEmail: any = '';
  public prodId: any = '';
  public prodName: any = '';
  public prodNewName: any = '';
  public products: any[] = [];
  public unselectIcon: any = null;

  /* Private fields */
  private getIconZip: any = null;

  constructor(private ref: ChangeDetectorRef, private _http: Http) {
    const self = this;
    DropPanelComponent.sohoEngine = new SohoEngineComponent(_http);
    this.getIconSrc = DropPanelComponent.sohoEngine.getIconSrc;
    this.getIconZip = DropPanelComponent.sohoEngine.getIconZip;
    DropPanelComponent.sohoEngine.refreshIconCart = function(icons) {
      self.toggleLabel((DropPanelComponent.sohoEngine.isShowCart ? 'show' : 'hide'));
      self.selectedIcons = icons;
      self.ref.detectChanges();
    };

    this.clearAll = DropPanelComponent.clearAll;
    this.unselectIcon = DropPanelComponent.unselectIcon;
  }

  /* Static Function */
  static clearAll() {
    const tempSelectedIcons = [];

    let len = this.selectedIcons.length;
    for (let i = 0; i < len; i++) {
      const icon = this.selectedIcons[i];

      tempSelectedIcons.push(icon.iconId);
    }

    len = tempSelectedIcons.length;
    for (let i = 0; i < len; i++) {
      const iconId = tempSelectedIcons[i];

      this.unselectIcon(iconId);
    }

    IconViewComponent.remoteEnableSingleDownload();
    IconViewComponent.remoteToggleSelectText();
  }

  static remoteToggleLabel(isHide) {
    if (isHide) {
      this.dropPanelLabel = 'Hide';
    } else {
      this.dropPanelLabel = 'Show';
    }
  }

  static unselectIcon(id) {
    let index = 0;
    const len = this.selectedIcons.length;
    for (let i = 0; i < len; i++) {
      const icon = this.selectedIcons[i];

      if (icon.iconId === id) {
        index = i;
        break;
      }
    }

    $('#cb-' + id).prop('checked', false);

    this.selectedIcons.splice(index, 1);
    DropPanelComponent.sohoEngine.unselectIcon(id);

    if (this.selectedIcons.length === 0) {
      IconViewComponent.remoteEnableSingleDownload();
    }
  }

  /* Implemented functions */
  checkNoChanges() { }
  detach() { }
  detectChanges() { }
  markForCheck() { }
  ngOnInit() { }
  ngOnChanges() { }
  reattach() { }

  /* Public functions */
  public downloadAllIcon(icon, fieldVal) {
    this.prodEmail = fieldVal.prodEmail;
    this.prodId = fieldVal.prodId;
    this.isNewProduct = fieldVal.isNewProduct;
    this.prodNewName = fieldVal.prodNewName;

    const self = this;
    const svc = DropPanelComponent.sohoEngine.getDownloadUpdate(icon, this.prodEmail, this.prodId,
                                  this.isNewProduct, this.prodNewName);
    svc.map((res: Response) => res.json())
    .subscribe(data => {
      self.downloadIcon(data);
    });

    DropPanelComponent.clearAll();
  }

  public downloadIcon(data) {
    window.open(SohoEngineComponent.serverAddress + '/getIconPack?zipPath=' + data.iconZip.zipPath);

    DropPanelComponent.clearAll();
  }

  public getIconDownload(icon) {
    let dlName = '';

    if (icon) {
      dlName += icon.iconName;
      dlName += '.zip';
    }

    return dlName;
  }

  public showProductOwner() {
    DropPanelComponent.sohoEngine.downloadIcon = this.downloadIcon;
    DropPanelComponent.sohoEngine.IconsToDownload = this.selectedIcons;
    DropPanelComponent.sohoEngine.DownloadType = 'drop-panel';
    DropPanelComponent.sohoEngine.showModal();
  }

  public toggleLabel(cmd) {
    if (cmd === 'hide') {
      DropPanelComponent.sohoEngine.isShowCart = false;
      this.dropPanelLabel = 'Show';
    } else if (cmd === 'show') {
      DropPanelComponent.sohoEngine.isShowCart = true;
      this.dropPanelLabel = 'Hide';
    } else if (this.dropPanelLabel === 'Show') {
      DropPanelComponent.sohoEngine.isShowCart = true;
      this.dropPanelLabel = 'Hide';
    } else if (this.dropPanelLabel === 'Hide') {
      DropPanelComponent.sohoEngine.isShowCart = false;
      this.dropPanelLabel = 'Show';
    }
  }

  public toggleNewProduct2() {
    this.isNewProduct = !this.isNewProduct;
  }
}
