import { Component, OnChanges, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, HostBinding } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';

import { SohoEngineComponent } from '../../soho-engine/soho-engine.component';
import { debug } from 'util';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnChanges, OnInit, ChangeDetectorRef {
  @HostBinding('class.header') get isHeader() { return true; }
  @HostBinding('class.is-personalizable') get isPersonalizable() { return true; }

  /* Static Fields */
  static mainTitle: any = 'Icons';
  get mainTitle(): any { return HeaderComponent.mainTitle; }
  set mainTitle(val: any) { HeaderComponent.mainTitle = val; }
  static accessPage: any = 'front';
  get accessPage(): any { return HeaderComponent.accessPage; }
  set accessPage(val: any) { HeaderComponent.accessPage = val; }

  /* Private fields */
  private accessLevel: any = '2';
  
  private sohoEngine: SohoEngineComponent = null;

  /* Public properties */
  public searchKey: any = '';
  public openNewTab = null;

  constructor(private ref: ChangeDetectorRef, private _http: Http, private router: Router) {
    const self = this;
    this.sohoEngine = new SohoEngineComponent(_http);

    this.accessLevel = this.sohoEngine.AccessLevel;

    this.openNewTab = this.sohoEngine.openNewTab;

    this.accessPage = 'front';

    if (this.getAccessLevel() === '0') {
      this.accessPage = 'admin';
    }
  }

  /* Static Functions */
  static setMainTitle(title) {
    this.mainTitle = title;
  }

  /* Implemented functions */
  checkNoChanges() { }
  detach() { }
  detectChanges() { }
  markForCheck() { }
  ngOnInit() { }
  ngOnChanges() { }
  reattach() { }

  /* Public functions */
  public searchIcons() {
    this.sohoEngine.IconFilteredList = this.sohoEngine.filterIcons(this.searchKey, '');
  }

  public goToAddIconView() {
    this.accessLevel = this.sohoEngine.getAccessLevel();
    this.changeLocalPage();
    this.router.navigateByUrl('hnl-dev/2.1.1/app-admin-icon' + '?al=' + this.accessLevel);
  }

  public getAccessLevel() {
    const url_string = this.getRootLink();
    const url = new URL(url_string);
    const accessLevel = url.searchParams.get('al');

    return accessLevel;
  }

  public getRootLink() {
    return window.location.href;
  }

  public changeLocalPage() {
    if (this.getAccessLevel() === '0') {
      if (this.accessPage === 'admin') {
        this.accessPage = 'front';
      } else {
        this.accessPage = 'admin';
      }
    } else {
      this.accessPage = 'front';
    }
  }

  static changeAccessPage(forceAdmin) {
    const url_string = window.location.href;
    const url = new URL(url_string);
    const accessLevel = url.searchParams.get('al');

    if (forceAdmin) {
      this.accessPage = 'admin';
    } else {
      if (accessLevel === '0') {
        if (this.accessPage === 'admin') {
          this.accessPage = 'front';
        } else {
          this.accessPage = 'admin';
        }
      } else {
        this.accessPage = 'front';
      }
    }
  }
}
