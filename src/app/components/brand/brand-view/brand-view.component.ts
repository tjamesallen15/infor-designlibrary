import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-brand-view',
  templateUrl: './brand-view.component.html',
  styleUrls: ['./brand-view.component.css']
})
export class BrandViewComponent implements OnInit {
  public under_construction = 'assets/under-construction.png';
  
  constructor() { }

  ngOnInit() {
  }

}
