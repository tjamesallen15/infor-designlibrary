import { Component, OnInit, AfterViewInit, NgZone } from '@angular/core';
import { Http, Response } from '@angular/http';

import { SohoMessageService, SohoMessageRef } from '@infor/sohoxi-angular';
import { Router } from '@angular/router';

import { HeaderComponent } from '../../templates/header/header.component';

@Component({
  selector: 'app-admin-icon',
  templateUrl: './admin-icon.component.html',
  styleUrls: ['./admin-icon.component.css'],
})
export class AdminIconComponent implements OnInit, AfterViewInit {
  /* Server Info Fields */
  private backEndVersion = '-2.1.1';
  private serverAddress = 'http://phmavwhnl.infor.com:9090/DesignSpring' + this.backEndVersion; /* DEV Instance */
  /* private serverAddress = 'http://phmavwhnl.infor.com:9190/DesignSpring' + this.backEndVersion; /* QA Instance */
  /* private serverAddress = 'http://phmavwhnl.infor.com:9290/DesignSpring' + this.backEndVersion;  /* PRD Instance */
  /* private serverAddress = 'http://10.88.64.136:8080/DesignSpring'; /* Test Instance */

  /* Public properties */
  public icons: any[] = [];
  public types: any[] = [];
  public products: any[] = [];
  public tagList: any = [];
  public productList: any = [];

  public iconInfoId: any = '';
  public iconInfoName: any = '';
  public iconInfoType: any = '';
  public iconInfoProduct: any = '';
  public iconPropertySize: any = '';
  public iconPropertyFormat: any = '';
  public iconPropertyPrefix: any = '';
  public iconTagField: any = '';
  public iconProductField: any = '';

  public hasPng: boolean = true;
  public defHasName: boolean = false;
  public defHasType: boolean = false;
  public defHasProduct: boolean = false;
  public defHasSvg: boolean = false;
  public defHasPng: boolean = false;
  public defHas18: boolean = false;
  public defHas22: boolean = false;
  public defHas29: boolean = false;
  public defHas40: boolean = false;
  public defHas48: boolean = false;
  public defHas50: boolean = false;
  public defHas57: boolean = false;
  public defHas58: boolean = false;
  public defHas72: boolean = false;
  public defHas76: boolean = false;
  public defHas80: boolean = false;
  public defHas100: boolean = false;
  public defHas114: boolean = false;
  public defHas120: boolean = false;
  public defHas144: boolean = false;
  public defHas152: boolean = false;
  public defHas512: boolean = false;
  public defHas1024: boolean = false;
  public iconImage2: boolean = false;
  public iconImage3: boolean = false;
  public iconImage4: boolean = false;
  public disableAddMore: boolean = false;

  public iconImageCount: any = 1;
  public adminMode: any = 'add';
  public iconDisplay: any = '';
  public idSvg = 'svg';
  public idPng = 'png';
  public id18 = '18';
  public id22 = '22';
  public id29 = '29';
  public id40 = '40';
  public id48 = '48';
  public id50 = '50';
  public id57 = '57';
  public id58 = '58';
  public id72 = '72';
  public id76 = '76';
  public id80 = '80';
  public id100 = '100';
  public id114 = '114';
  public id120 = '120';
  public id144 = '144';
  public id152 = '152';
  public id512 = '512';
  public id1024 = '1024';
  public idNoProduct = 'NoProduct';

  public model = {
    idNoProductValue: false,
    idSvgValue: false,
    idPngValue: false,
    id18Value: false,
    id22Value: false,
    id29Value: false,
    id40Value: false,
    id48Value: false,
    id50Value: false,
    id57Value: false,
    id58Value: false,
    id72Value: false,
    id76Value: false,
    id80Value: false,
    id100Value: false,
    id114Value: false,
    id120Value: false,
    id144Value: false,
    id152Value: false,
    id512Value: false,
    id1024Value: false
  };

  /* Private fields */
  private dialog: SohoMessageRef = null;
  private closeResult: string;

  constructor(private _http: Http, private messageService: SohoMessageService, private _ngZone: NgZone, private router: Router) {
    const self = this;

    if (self.getIconId()) {
      self.adminMode = 'edit';
      self.getIconInfoById(self.getIconId());
    }
    self.getTypes();
    self.getProducts();
  }

  /* Implemented functions */
  ngOnInit() { }
  ngAfterViewInit() {
    this.fieldActivation();
  }

  /* Public functions */
  public getAccessLevel() {
    const url_string = this.getRootLink();
    const url = new URL(url_string);
    const accessLevel = url.searchParams.get('al');

    return accessLevel;
  }

  public getSubmitActivation() {
    if (this.iconInfoName !==  '' && this.iconInfoType !==  '') {
      switch (this.adminMode) {
        case 'add' :
          if (this.model.idSvgValue || this.model.idPngValue) {
            if (this.haveFilesReadyForUpload() && this.haveFileDimensionSelected()) {
              if (this.iconTagField !==  '') {
                return false;
              } else {
                return true;
              }
            } else {
              return true;
            }
          } else {
            return true;
          }
        case 'edit' :
          if (this.iconProductField) {
            return false;
          }
          
          if (this.iconTagField !==  '') {
            return false;
          }

          return true;
      }
    } else {
      return true;
    }
  }

  public haveFilesReadyForUpload() {
    if ($('#fileupload1').prop('files').length > 0 || $('#fileupload2').prop('files').length > 0
      || $('#fileupload3').prop('files').length > 0 || $('#fileupload4').prop('files').length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public haveFileDimensionSelected() {
    if (this.model.idPngValue) {
      let passValidation = false;
      for (const key of Object.keys(this.model)) {
        if (key !== 'idNoProductValue' && key !== 'idSvgValue' && key !== 'idPngValue') {
          if(this.model[key]) {
            passValidation = true;
            break;
          }
        }
      }
      return passValidation;
    } else {
      return true;
    }
  }

  public fieldActivation() {
    let accessLevel = this.getAccessLevel();

    if (accessLevel === '1') {
      $('#propertyBorder').addClass('hidden-admin-border');
      $('#tagBorder').addClass('hidden-admin-border');
    }
  }

  public addMoreIconImage() {
    if (this.iconImageCount == 1) {
      this.iconImage2 = true;
    } else if (this.iconImageCount == 2) {
      this.iconImage3 = true;
    } else if (this.iconImageCount == 3) {
      this.iconImage4 = true;
      this.disableAddMore = true;
    }
    this.iconImageCount++;
  }

  public capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  public getIconId() {
    const url_string = this.getRootLink();
    const url = new URL(url_string);
    const iconId = url.searchParams.get('iconId');

    return iconId;
  }

  public getIconDisplay() {
    return this.iconDisplay;
  }

  public getRootLink() {
    return window.location.href;
  }

  public openInfoMessage(message) {
    const buttons = [
      {
        text: 'OK',
        click: (e, modal) => {
          this.closeResult = 'OK';
          this.dialog = null;
          modal.close(true);
          HeaderComponent.changeAccessPage(false);
          window.history.back();
        }
      }
    ];

    this.dialog = this.messageService
    .message()
    .title('<span>Admin Information</span>')
    .message('<span class="longer-message">' + message + '</span>')
    .buttons(buttons)
    .open();
  }

  public goBack() {
    this.router.navigateByUrl('hnl-dev/2.1.1/app-icon-view' + '?al=' + this.getAccessLevel());
    HeaderComponent.changeAccessPage(false);
  }

  public validateProductsAndTags() {
    let validator = 'pass';

    for (let i = 0; i < this.productList.length; i++) {
      let checker = this.productList[i];
      if (checker !== '' && checker === this.iconProductField) {
        validator = '1';
        break;
      }
    }

    return validator;
  }

  public saveIcon() {
    let validation = this.validateProductsAndTags();
    // Create Object for Sending
    if (validation === 'pass') {
      let iconData = '';
      let hasNewSvg = 'N';
      let hasNewPng = 'N';

      if (this.iconInfoId) {
        iconData += 'iconId:' + this.iconInfoId + ';';
      }

      if (this.iconInfoName && this.iconInfoType) {
        iconData += 'iconName:' + this.iconInfoName + ';';
        iconData += 'iconType:' + this.iconInfoType + ';';
      }

      if (this.iconProductField) {
        iconData += 'hasProductNew:' + 'Y' + ';';
        iconData += 'productNew:' + this.iconProductField + ';';
      } else {
        iconData += 'hasProductNew:' + 'N' + ';'
        iconData += 'productId:' + this.iconInfoProduct + ';';
      }

      if (this.model.idSvgValue && !this.defHasSvg) {
        iconData += 'hasSvg:' + 'Y' + ';';
        hasNewSvg = 'Y';
      }

      if (this.model.idPngValue) {
        iconData += 'hasPng:' + 'Y' + ';';

        if (this.model.id18Value && !this.defHas18) {
          iconData += 's18:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id22Value && !this.defHas22) {
          iconData += 's22:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id29Value && !this.defHas29) {
          iconData += 's29:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id40Value && !this.defHas40) {
          iconData += 's40:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id48Value && !this.defHas48) {
          iconData += 's48:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id50Value && !this.defHas50) {
          iconData += 's50:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id57Value && !this.defHas57) {
          iconData += 's57:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id58Value && !this.defHas58) {
          iconData += 's58:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id72Value && !this.defHas72) {
          iconData += 's72:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id76Value && !this.defHas76) {
          iconData += 's76:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id80Value && !this.defHas80) {
          iconData += 's80:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id100Value && !this.defHas100) {
          iconData += 's100:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id114Value && !this.defHas114) {
          iconData += 's114:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id120Value && !this.defHas120) {
          iconData += 's120:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id144Value && !this.defHas144) {
          iconData += 's144:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id152Value && !this.defHas152) {
          iconData += 's152:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id512Value && !this.defHas512) {
          iconData += 's512:' + 'Y' + ';';
          hasNewPng = 'Y';
        }

        if (this.model.id1024Value && !this.defHas1024) {
          iconData += 's1024:' + 'Y' + ';';
          hasNewPng = 'Y';
        }
      }

      if (this.iconTagField) {
        iconData += 'tags:' + this.iconTagField + ';';
      }

      if (hasNewSvg) {
        iconData += 'hasNewSvg:' + hasNewSvg + ';'
      }

      if (hasNewPng) {
        iconData += 'hasNewPng:' + hasNewPng + ';'
      }

      const formData = new FormData();
      formData.append('mode', this.adminMode);
      formData.append('iconData', iconData);

      if ($('#fileupload1').prop('files') && $('#fileupload1').prop('files').length) {
        const fileData1 = $('#fileupload1').prop('files')[0];
        
        formData.append('iconImage1', fileData1);
      } else {
        formData.append('iconImage1', 'EMPTY');
      }

      if ($('#fileupload2').prop('files') && $('#fileupload2').prop('files').length) {
        const fileData2 = $('#fileupload2').prop('files')[0];
        formData.append('iconImage2', fileData2);
      } else {
        formData.append('iconImage2', 'EMPTY');
      }

      if ($('#fileupload3').prop('files') && $('#fileupload3').prop('files').length) {
        const fileData3 = $('#fileupload3').prop('files')[0];
        formData.append('iconImage3', fileData3);
      } else {
        formData.append('iconImage3', 'EMPTY');
      }

      if ($('#fileupload4').prop('files') && $('#fileupload4').prop('files').length) {
        const fileData4 = $('#fileupload4').prop('files')[0];
        formData.append('iconImage4', fileData4);
      } else {
        formData.append('iconImage4', 'EMPTY');
      }

      const self = this;
      $.ajax({
        url: this.serverAddress + '/saveIcon',
        type: 'POST',
        data: formData,
        dataType: 'text',
        async: false,
        contentType: false,
        processData: false,
        cache: false,
        success: function(callback) {
          const success = JSON.parse(callback);
          if (success.response == 0) {
            self.openInfoMessage(success.errorMessage);
          } else {
            if (self.adminMode === 'add') {
              self.openInfoMessage('The icon has been saved succesfully! Newly added icons need to be reviewed by administrators before becoming available for viewing and download.');
            } else {
              self.openInfoMessage('The icon has been saved succesfully!');
            }
          }
        },
        error: function() {
          self.openInfoMessage('The Web Service is unreachable. Unable to create record.');
        }
      });
    } else {
      const setModal = function (opt) {
        opt = $.extend({
          buttons: [{
            text: 'OK',
            click: function(e, modal) {
              modal.close();
            }
          }]
        }, opt);

        $('#modal-product-validation').modal(opt);
      };

      setModal({
        'title': 'Warning!',
        'isAlert': true,
        'content': $('#modal-product-validation')
      });
    }
  }

  public validatePngFormats() {
    if (this.hasPng) {
      this.hasPng = false;
    } else {
      this.hasPng = true;
    }
  }

  /* WEB SERVICE CALLS */
  private getIconInfoById(iconId) {
    const formData = new FormData();
    formData.append('iconId', iconId);

    const self = this;

    $.ajax({
      url: this.serverAddress + '/getIconInfoById',
      type: 'POST',
      data: formData,
      dataType: 'text',
      contentType: false,
      processData: false,
      cache: false,
      success: function(response) {
        let data = JSON.parse(response);
        console.log(data);

        self.iconInfoId = data.iconId;
        if (data.iconName) {
          self.iconInfoName = data.iconName;
          self.defHasName = true;
        }

        self.iconDisplay = 'assets/icons/';
        self.iconDisplay += data.type.typeName + '/';
        self.iconDisplay += data.iconName + '/';
        self.iconDisplay += data.iconDisplayName;

        if (data.type.typeId) {
          self.iconInfoType = data.type.typeId;
          self.defHasType = true;
        }

        for (let i = 0; i < data.properties.length; i++) {
          let property = data.properties[i];
          if (property.propertyFormat === '.svg') {
              self.model.idSvgValue = true;
              self.defHasSvg = true;
          } else if (property.propertyFormat === '.png') {
            self.model.idPngValue = true;
            self.hasPng = false;
            self.defHasPng = true;
            if (property.propertySize === '18x18') {
              self.model.id18Value = true;
              self.defHas18 = true;
            } else if (property.propertySize === '22x22') {
              self.model.id22Value = true;
              self.defHas22 = true;
            } else if (property.propertySize === '29x29') {
              self.model.id29Value = true;
              self.defHas29 = true;
            } else if (property.propertySize === '40x40') {
              self.model.id40Value = true;
              self.defHas40 = true;
            } else if (property.propertySize === '48x48') {
              self.model.id48Value = true;
              self.defHas48 = true;
            } else if (property.propertySize === '50x50') {
              self.model.id50Value = true;
              self.defHas50 = true;
            } else if (property.propertySize === '57x57') {
              self.model.id57Value = true;
              self.defHas57 = true;
            } else if (property.propertySize === '58x58') {
              self.model.id58Value = true;
              self.defHas58 = true;
            } else if (property.propertySize === '72x72') {
              self.model.id72Value = true;
              self.defHas72 = true;
            } else if (property.propertySize === '76x76') {
              self.model.id76Value = true;
              self.defHas76 = true;
            } else if (property.propertySize === '80x80') {
              self.model.id80Value = true;
              self.defHas80 = true;
            } else if (property.propertySize === '100x100') {
              self.model.id100Value = true;
              self.defHas100 = true;
            } else if (property.propertySize === '114x114') {
              self.model.id114Value = true;
              self.defHas114 = true;
            } else if (property.propertySize === '120x120') {
              self.model.id120Value = true;
              self.defHas120 = true;
            } else if (property.propertySize === '144x144') {
              self.model.id144Value = true;
              self.defHas144 = true;
            } else if (property.propertySize === '152x152') {
              self.model.id152Value = true;
              self.defHas152 = true;
            } else if (property.propertySize === '512x512') {
              self.model.id512Value = true;
              self.defHas512 = true;
            } else if (property.propertySize === '1024x1024') {
              self.model.id1024Value = true;
              self.defHas1024 = true;
            }
          }
        }

        for (let j = 0; j < data.tags.length; j++) {
          self.tagList.push(data.tags[j].tagName);
        }

        if (data.products && data.products.length) {
          self.defHasProduct = true;
        }

        for (let k = 0; k < data.products.length; k++) {
          self.productList.push(data.products[k].productName);
          for (let z = 0; z < self.products.length; z++) {
            if (self.products[z].productName === data.products[k].productName) {
            console.log(self.products[z].productName);
              self.products.splice(z, 1);
            }
          }
        }

      },
      error: function() {
      }
    });
  }

  private getTypes() {
    const service = this._http.get(this.serverAddress + '/getTypes')
                  .map((res: Response) => res.json())
                  .subscribe(data => {
                    this.types = data;
                    this.types.shift();
                    service.unsubscribe();
                  });

    return service;
  }

  private getProducts() {
    const service = this._http.get(this.serverAddress + '/getProducts')
                  .map((res: Response) => res.json())
                  .subscribe(data => {
                    this.products = data;
                    service.unsubscribe();
                  });

    return service;
  }

}
