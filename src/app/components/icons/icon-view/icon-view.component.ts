import { Component, OnChanges, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';

import { SohoEngineComponent } from '../../soho-engine/soho-engine.component';
import { DropPanelComponent } from '../../templates/drop-panel/drop-panel.component';
import { HeaderComponent } from '../../templates/header/header.component';

@Component({
  selector: 'app-icon-view',
  templateUrl: './icon-view.component.html',
  styleUrls: ['./icon-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconViewComponent implements OnChanges, OnInit, ChangeDetectorRef {
  /* Static Fields */
  static isAllowSingleDownload: any = true;
  get isAllowSingleDownload(): any { return IconViewComponent.isAllowSingleDownload; }
  set isAllowSingleDownload(val: any) { IconViewComponent.isAllowSingleDownload = val; }
  static selectText: String = 'Select All';
  get selectText(): any { return IconViewComponent.selectText; }
  set selectText(val: any) { IconViewComponent.selectText = val; }
  static sohoEngine: SohoEngineComponent = null;
  get sohoEngine(): any { return IconViewComponent.sohoEngine; }
  set sohoEngine(val: any) { IconViewComponent.sohoEngine = val; }
  static accessLevel: any = '2';
  get accessLevel(): any { return IconViewComponent.accessLevel; }
  set accessLevel(val: any) { IconViewComponent.accessLevel = val; }
  static getIconSrc: any = null;
  get getIconSrc(): any { return IconViewComponent.getIconSrc; }
  set getIconSrc(val: any) { IconViewComponent.getIconSrc = val; }
  static getIconZip: any = null;
  get getIconZip(): any { return IconViewComponent.getIconZip; }
  set getIconZip(val: any) { IconViewComponent.getIconZip = val; }
  static getSidePanelIconSrc: any = null;
  get getSidePanelIconSrc(): any { return IconViewComponent.getSidePanelIconSrc; }
  set getSidePanelIconSrc(val: any) { IconViewComponent.getSidePanelIconSrc = val; }
  static icons: any[] = [];
  get icons(): any { return IconViewComponent.icons; }
  set icons(val: any) { IconViewComponent.icons = val; }
  static isSearch: any = false;
  get isSearch(): any { return IconViewComponent.isSearch; }
  set isSearch(val: any) { IconViewComponent.isSearch = val; }
  static selectedIcon: any = null;
  get selectedIcon(): any { return IconViewComponent.selectedIcon; }
  set selectedIcon(val: any) { IconViewComponent.selectedIcon = val; }
  static filterCount: any = 0;
  get filterCount(): any { return IconViewComponent.filterCount; }
  set filterCount(val: any) { IconViewComponent.filterCount = val; }
  static filterType: any = 'all';
  get filterType(): any { return IconViewComponent.filterType; }
  set filterType(val: any) { IconViewComponent.filterType = val; }
  static products: any[] = [];
  get products(): any { return IconViewComponent.products; }
  set products(val: any) { IconViewComponent.products = val; }
  static filteredProducts: any = [];
  get filteredProducts(): any { return IconViewComponent.filteredProducts; }
  set filteredProducts(val: any) { IconViewComponent.filteredProducts = val; }
  static isNoResult: any = false;
  get isNoResult(): any { return IconViewComponent.isNoResult; }
  set isNoResult(val: any) { IconViewComponent.isNoResult = val; }
  static Ref: any = null;
  get Ref(): any { return IconViewComponent.Ref; }
  set Ref(val: any) { IconViewComponent.Ref = val; }
  static Http: any = null;
  get Http(): any { return IconViewComponent.Http; }
  set Http(val: any) { IconViewComponent.Http = val; }
  static isFocused: any = false;
  get isFocused(): any { return IconViewComponent.isFocused; }
  set isFocused(val: any) { IconViewComponent.isFocused = val; }

  /* Public properties */
  public iconView: String = 'grid';
  public prodIcons: any[] = [];
  public isNewProduct: any = false;
  public prodEmail: any = '';
  public prodId: any = '';
  public prodName: any = '';
  public prodNewName: any = '';
  public searchKey: any = '';

  /* Private properties */

  constructor(private ref: ChangeDetectorRef, private _http: Http, private router: Router) {
    IconViewComponent.Ref = ref;
    IconViewComponent.Http = _http;

    IconViewComponent.refreshData(false);
  }

  /* Static Function */
  static refreshData(dontRefresh) {
    const self = this;
    IconViewComponent.sohoEngine = new SohoEngineComponent(IconViewComponent.Http);
    IconViewComponent.accessLevel = IconViewComponent.sohoEngine.AccessLevel;
    IconViewComponent.getIconSrc = IconViewComponent.sohoEngine.getIconSrc;
    IconViewComponent.getIconZip = IconViewComponent.sohoEngine.getIconZip;
    IconViewComponent.getSidePanelIconSrc = IconViewComponent.sohoEngine.getSidePanelIconSrc;

    /* Callbacks */
    IconViewComponent.sohoEngine.afterGetIcons = function(icons) {
      IconViewComponent.icons = !dontRefresh ? IconViewComponent.formatIconList(icons) : IconViewComponent.icons;
      IconViewComponent.selectedIcon = !dontRefresh ? icons.length > 0 ? icons[0] : {} : IconViewComponent.selectedIcon;
      IconViewComponent.filterCount = SohoEngineComponent.IconFilteredCount;
      self.filterType = SohoEngineComponent.FilterType;

      IconViewComponent.Ref.detectChanges();
    };

    IconViewComponent.sohoEngine.afterGetProducts = function(products) {
      IconViewComponent.products = products;
      self.filteredProducts = products;

      if (!self.isFocused) {
        $('#searchfield').focus();
        self.isFocused = true;
      }

      IconViewComponent.Ref.detectChanges();
    };
  }

  static remoteEnableSingleDownload() {
    this.isAllowSingleDownload = true;
  }

  static remoteToggleSelectText() {
    const len = this.icons.length;
    for (let i = 0; i < len; i++) {
      const ic = this.icons[i];

      ic.selectText = 'Select All';
    }
  }

  static formatIconList(iconList) {
    const len = iconList.length;
    for (let i = 0; i < len; i++) {
      const icon = iconList[i];
      icon.displayFormats = [];

      const len2 = icon.properties.length;
      for (let ii = 0; ii < len2; ii++) {
        const prop = icon.properties[ii];

        let isExist = false;
        const len3 = icon.displayFormats.length;
        for (let iii = 0; iii < len3; iii++) {
          const dFormat = icon.displayFormats[iii];

          if (dFormat.format === prop.propertyFormat.slice(1)) {
            isExist = true;
          }
        }

        if (!isExist) {
          icon.displayFormats.push({ format: prop.propertyFormat.slice(1) });
        }
      }
    }

    this.isNoResult = iconList.length === 0;

    return this.organizeCategories(iconList);
  }

  static organizeCategories(iconList) {
    const tempList = IconViewComponent.sohoEngine.IconTypeList;
    let len = tempList.length;
    for (let i = 0; i < len; i++) {
      const item = tempList[i];

      item.icons = [];
      item.selectText = 'Select All';
    }

    len = iconList.length;
    for (let i = 0; i < len; i++) {
      const icon = iconList[i];

      const len2 = tempList.length;
      for (let ii = 0; ii < len2; ii++) {
        const item = tempList[ii];

        if (icon.type.typeName === item.typeName) {
          item.icons.push(icon);
        }
      }
    }

    return tempList;
  }

  /* Implemented functions */
  checkNoChanges() { }
  detach() { }
  detectChanges() { }
  markForCheck() { }
  ngOnInit() { }
  ngOnChanges() { }
  reattach() { }

  /* Public functions */
  public capitalizeFirstLetter(str) {
    return IconViewComponent.sohoEngine.capitalizeFirstLetter(str);
  }

  public copySVGCode(icon) {
  }

  public downloadIcon(icon) {
    const sidebarDLLink = document.getElementById('sidebar-download-link');
    const rootLink = IconViewComponent.sohoEngine.getRootLink();

    sidebarDLLink.setAttribute('href', rootLink + this.getIconZip(icon));
    sidebarDLLink.setAttribute('download', this.getIconDownload(icon));

    sidebarDLLink.click();

    /* Clear Info? */
    // this.prodEmail = '';
    // this.prodId = '';
    // this.isNewProduct = '';
    // this.prodNewName = '';
  }

  public downloadProdIcon() {
    const len = this.prodIcons.length;
    for (let i = 0; i < len; i++) {
      const icon = this.prodIcons[i];

      this.downloadIcon(icon);
    }
  }

  public filterProducts(ev) {
    const x = 0;
  }

  public getIconDownload(icon) {
    let dlName = '';

    if (icon) {
      dlName += icon.iconName;
      dlName += '.zip';
    }

    return dlName;
  }

  public iconMouseEnter(ev, icon) {
    icon.visible = true;
  }

  public iconMouseLeave(ev, icon) {
    if (!icon.isSelected) {
      icon.visible = false;
    }
  }

  public isAllowGridView() {
    return (this.iconView === 'grid') ? true : false;
  }

  public isAllowShowCategory(iconCat) {
    if (iconCat.icons.length) {
      return true;
    }

    return false;
  }

  public isIconPopulated() {
    let isPopulated = false;

    const len = this.icons.length;
    for (let i = 0; i < len; i++) {
      const iconCat = this.icons[i];

      if (iconCat.icons.length > 0) {
        isPopulated = true;
        break;
      }
    }

    return isPopulated;
  }

  public onSearchKeyup(ev, key) {
    this.searchKey = key;

    if (ev.which === 13) {
      this.searchIcons();
    }
  }

  public searchIcons() {
    const self = this;
    IconViewComponent.isSearch = this.searchKey;
    IconViewComponent.sohoEngine.filterIcons(this.searchKey, '');

    // setTimeout(function() {
    //   if (self.icons.length && self.icons[0].icons.length) {
    //     self.selectIcon(null, self.icons[0].icons[0]);
    //   }      
    // }, 500);
  }

  public selectAllIcon(iconCategory) {
    if (iconCategory.selectText === 'Select All') {
      iconCategory.selectText = 'Unselect All';
      this.isAllowSingleDownload = false;
    } else {
      iconCategory.selectText = 'Select All';

      DropPanelComponent.remoteToggleLabel(false);
      this.isAllowSingleDownload = true;
    }

    const tempSelectedIcons = [];

    const len = this.icons.length;
    for (let i = 0; i < len; i++) {
      const iconCat = this.icons[i];

      const len2 = iconCat.icons.length;
      for (let ii = 0; ii < len2; ii++) {
        const ic = iconCat.icons[ii];
        if (ic.type.typeName === iconCategory.typeName && ic.type.typeName !== 'badge') {
          if (iconCategory.selectText === 'Select All') {
            ic.isSelected = false;
            ic.visible = false;
          } else {
            ic.isSelected = true;
            ic.visible = true;
            tempSelectedIcons.push(ic);
          }
        } else if (ic.isSelected) {
          tempSelectedIcons.push(ic);
        }
        $('#cb-' + ic.iconId).prop('checked', ic.isSelected);
      }
    }

    IconViewComponent.sohoEngine.SelectedIconList = tempSelectedIcons;
    IconViewComponent.sohoEngine.refreshIconCart(tempSelectedIcons);

    if (IconViewComponent.sohoEngine.isShowCart) {
      $('#drop-expandable-area').find('.expandable-area').addClass('is-expanded');
      $('#drop-expandable-area').find('.expandable-pane').css({
        'display': 'block',
        'height': 'auto'
      });

      DropPanelComponent.remoteToggleLabel(true);
    } else {
      DropPanelComponent.remoteToggleLabel(false);
    }
  }

  public selectIcon(ev, icon) {
    this.selectedIcon = icon;

    $('.ui-item').removeClass('is-active');
    $('#ii-' + icon.iconId).addClass('is-active');
  }

  public selectMultipleIcon(ev, icon, iconCategory) {
    icon.isSelected = !icon.isSelected;

    const tempSelectedIcons = [];

    const len = this.icons.length;
    for (let i = 0; i < len; i++) {
      const iconCat = this.icons[i];

      const iconTotalCount = iconCat.icons.length;
      const tempCategoryIcons = [];

      const len2 = iconCat.icons.length;
      for (let ii = 0; ii < len2; ii++) {
        const ic = iconCat.icons[ii];

        if (ic.isSelected) {
          tempSelectedIcons.push(ic);
          tempCategoryIcons.push(ic);
        }
      }

      if (iconTotalCount > tempCategoryIcons.length) {
        iconCat.selectText = 'Select All';
        $('#select-all-' + iconCat.typeId).prop('checked', false);
      } else {
        iconCat.selectText = 'Unselect All';
        $('#select-all-' + iconCat.typeId).prop('checked', true);
      }
    }

    this.isAllowSingleDownload = tempSelectedIcons.length === 0;

    IconViewComponent.sohoEngine.SelectedIconList = tempSelectedIcons;
    IconViewComponent.sohoEngine.refreshIconCart(tempSelectedIcons);

    if (IconViewComponent.sohoEngine.isShowCart) {
      $('#drop-expandable-area').find('.expandable-area').addClass('is-expanded');
      $('#drop-expandable-area').find('.expandable-pane').css({
        'display': 'block',
        'height': 'auto'
      });

      DropPanelComponent.remoteToggleLabel(true);
    } else {
      DropPanelComponent.remoteToggleLabel(false);
    }
  }

  public showProductIcons (key) {
    this.prodIcons = IconViewComponent.sohoEngine.getProductIcons(key);
    const prodIconElements = this.buildProdIconElements(this.prodIcons);

    $('body').contextualactionpanel({
      title: key,
      content: '<div style="width:750px;">' + prodIconElements + '</div>',
      trigger: 'immediate',
      buttons: [
        {
          text: 'Download All',
          cssClass: 'btn'
        },
        {
          cssClass: 'separator'
        },
        {
          text: 'Close',
          cssClass: 'btn',
          icon: '#icon-close'
        }
      ]
    });

    const self = this;
    $('.contextual-action-panel .btn').on('click', function() {
      if (this.innerText === 'Download All') {
        self.downloadProdIcon();
      }
    });
  }

  public showProductOwner(icon) {
    IconViewComponent.sohoEngine.downloadIcon = this.downloadIcon;
    IconViewComponent.sohoEngine.IconsToDownload = [icon];
    IconViewComponent.sohoEngine.DownloadType = 'icon-view';
    IconViewComponent.sohoEngine.showModal();
  }

  public sourceProducts = (term: string, response: any) => {
    response(term, this.products);
  }

  public toggleNewProduct() {
    this.isNewProduct = !this.isNewProduct;
  }

  public unselectIcon (id) {
    const len = this.icons.length;
    for (let i = 0; i < len; i++) {
      const iconCat = this.icons[i];
      let isIconFound = false;

      const len2 = iconCat.icons.length;
      for (let ii = 0; ii < len2; ii++) {
        const icon = iconCat.icons[ii];

        if (icon.iconId === id) {
          icon.isSelected = false;
          isIconFound = true;
          break;
        } else  if (id === 'all') {
          icon.isSelected = false;
        }
      }

      if (isIconFound) {
        break;
      }
    }
  }

  /* Private Functions */
  private buildProdIconElements(prodIcons) {
    let prodElements = '';

    const len = prodIcons.length;
    for (let i = 0; i < len; i++) {
      const prodIcon = prodIcons[i];

      prodElements += '<div class="icon-placeholder">';
      prodElements += '<img src="' + this.getIconSrc(prodIcon) + '" class="width18">';
      prodElements += '<p class="icon-name-small">' + prodIcon.iconName + '</p>';
      prodElements += '</div>';
    }

    return prodElements;
  }

  private findProduct(searchKey) {
    const len = this.products.length;
    for (let i = 0; i < len; i++) {
      const prod = this.products[i];

      if (prod.productName === searchKey) {
        return prod;
      }
    }

    return null;
  }

  private goToEditIconView(icon) {
    IconViewComponent.accessLevel = this.sohoEngine.getAccessLevel();
    HeaderComponent.changeAccessPage(false);
    this.router.navigateByUrl('hnl-dev/2.1.1/app-admin-icon?iconId=' + icon.iconId + '&al=' + IconViewComponent.accessLevel);
  }
}
