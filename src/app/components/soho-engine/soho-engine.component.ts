import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { DropPanelComponent } from '../templates/drop-panel/drop-panel.component';
import { IconViewComponent } from '../icons/icon-view/icon-view.component';
import { debug } from 'util';

@Component({
  selector: 'app-soho-engine',
  templateUrl: './soho-engine.component.html',
  styleUrls: ['./soho-engine.component.css']
})
export class SohoEngineComponent implements OnInit {
  /* Server Info Fields */
  static backEndVersion = '-2.1.1';
  static serverAddress = 'http://phmavwhnl.infor.com:9090/DesignSpring' + SohoEngineComponent.backEndVersion; /* DEV Instance */
  /* static serverAddress = 'http://phmavwhnl.infor.com:9190/DesignSpring' + SohoEngineComponent.backEndVersion; /* QA Instance */
  /* static serverAddress = 'http://phmavwhnl.infor.com:9290/DesignSpring' + SohoEngineComponent.backEndVersion;  /* PRD Instance */
  /* static serverAddress = 'http://10.88.65.100:8080/DesignSpring'; /* Test Instance */

  /* Static properties */
  static AccessLevel: any = 'guest';
  get AccessLevel(): any { return SohoEngineComponent.AccessLevel; }
  set AccessLevel(val: any) { SohoEngineComponent.AccessLevel = val; }
  static FilterType: any = 'all';
  get FilterType(): any { return SohoEngineComponent.FilterType; }
  set FilterType(val: any) { SohoEngineComponent.FilterType = val; }
  static IconMasterList: any[] = [];
  get IconMasterList(): any { return SohoEngineComponent.IconMasterList; }
  set IconMasterList(val: any) { SohoEngineComponent.IconMasterList = val; }
  static BaseIconList: any[] = [];
  get BaseIconList(): any { return SohoEngineComponent.BaseIconList; }
  set BaseIconList(val: any) { SohoEngineComponent.BaseIconList = val; }
  static IconTypeList: any[] = [];
  get IconTypeList(): any { return SohoEngineComponent.IconTypeList; }
  set IconTypeList(val: any) { SohoEngineComponent.IconTypeList = val; }
  static IconFilteredCount: any = 0;
  get IconFilteredCount(): any { return SohoEngineComponent.IconFilteredCount; }
  set IconFilteredCount(val: any) { SohoEngineComponent.IconFilteredCount = val; }
  static IconFilteredList: any[] = [];
  get IconFilteredList(): any { return SohoEngineComponent.IconFilteredList; }
  set IconFilteredList(val: any) { SohoEngineComponent.IconFilteredList = val; }
  static isShowCart: any = true;
  get isShowCart(): any { return SohoEngineComponent.isShowCart; }
  set isShowCart(val: any) { SohoEngineComponent.isShowCart = val; }
  static SelectedIconList: any[] = [];
  get SelectedIconList(): any { return SohoEngineComponent.SelectedIconList; }
  set SelectedIconList(val: any) { SohoEngineComponent.SelectedIconList = val; }
  static ProductIconList: any[] = [];
  get ProductIconList(): any { return SohoEngineComponent.ProductIconList; }
  set ProductIconList(val: any) { SohoEngineComponent.ProductIconList = val; }
  static ProductMasterList: any[] = [];
  get ProductMasterList(): any { return SohoEngineComponent.ProductMasterList; }
  set ProductMasterList(val: any) { SohoEngineComponent.ProductMasterList = val; }
  /* Download Params */
  static DownloadType: any[] = [];
  get DownloadType(): any { return SohoEngineComponent.DownloadType; }
  set DownloadType(val: any) { SohoEngineComponent.DownloadType = val; }
  static IconsToDownload: any[] = [];
  get IconsToDownload(): any { return SohoEngineComponent.IconsToDownload; }
  set IconsToDownload(val: any) { SohoEngineComponent.IconsToDownload = val; }

  /* Callbacks */
  static afterGetIcons: any = null;
  get afterGetIcons(): any { return SohoEngineComponent.afterGetIcons; }
  set afterGetIcons(val: any) { SohoEngineComponent.afterGetIcons = val; }
  static afterGetProducts: any = null;
  get afterGetProducts(): any { return SohoEngineComponent.afterGetProducts; }
  set afterGetProducts(val: any) { SohoEngineComponent.afterGetProducts = val; }
  static afterGetTypes: any = null;
  get afterGetTypes(): any { return SohoEngineComponent.afterGetTypes; }
  set afterGetTypes(val: any) { SohoEngineComponent.afterGetTypes = val; }
  static  downloadIcon: any = null;
  get  downloadIcon(): any { return SohoEngineComponent. downloadIcon; }
  set  downloadIcon(val: any) { SohoEngineComponent. downloadIcon = val; }
  static refreshIconCart: any = null;
  get refreshIconCart(): any { return SohoEngineComponent.refreshIconCart; }
  set refreshIconCart(val: any) { SohoEngineComponent.refreshIconCart = val; }

  /* Modal Properties */
  public cachedProdId: any = '';
  public isAllowModalDownload: any = false;
  public isNewProduct: any = false;
  public prodName: any = '';
  public prodEmail: any = '';
  public prodNewName: any = '';
  public prodId: any = '';

  constructor(private _http: Http) {
    this.getTypes();

    SohoEngineComponent.AccessLevel = this.getAccessLevel();
  }

  ngOnInit() { }

  /* Public functions */
  public capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  public checkModalFields() {
    this.isAllowModalDownload = this.prodEmail && (this.isNewProduct ? this.prodNewName : this.prodId);

    if (this.isAllowModalDownload) {
      $('#modal-product-owner').parents('.modal-content').find('#modal-button1').prop('disabled', false);
    } else {
      $('#modal-product-owner').parents('.modal-content').find('#modal-button1').prop('disabled', true);
    }
  }

  public filterIcons(key, type) {
    let filterResult: any[] = [];

    if (type) {
      const len = this.IconMasterList.length;
      for (let i = 0; i < len; i++) {
        const icon = this.IconMasterList[i];

        if (key === 'all' || key === '') {
          this.FilterType = 'all';
          filterResult = this.IconMasterList;
          break;
        } else if (type === 'name' && this.containsString(icon.iconName, key)) {
          this.FilterType = key;
          filterResult.push(icon);
        } else if (type === 'type' && this.containsString(icon.type.typeName, key)) {
          this.FilterType = key;
          filterResult.push(icon);
        } else if (type === 'tag' && this.containsTag(icon, key)) {
          this.FilterType = key;
          filterResult.push(icon);
        } else {
          this.FilterType = 'Search results for ' + key;
        }
      }

      this.IconFilteredList = filterResult;
    } else {
      const len = this.IconFilteredList.length;
      for (let i = 0; i < len; i++) {
        const icon = this.IconFilteredList[i];

        if (this.containsString(icon.iconName, key) || this.containsTag(icon, key)
              || this.containsString(icon.type.typeName, key) || this.containsProduct(icon, key)) {
          this.FilterType = 'Search results for ' + key;
          filterResult.push(icon);
        }
      }
    }

    this.IconFilteredCount = filterResult.length;
    this.afterGetIcons(filterResult);

    return filterResult;
  }

  public getAccessLevel() {
    const url_string = this.getRootLink();
    const url = new URL(url_string);
    const accessLevel = url.searchParams.get('al');

    return accessLevel;
  }

  public getIconSrc(icon) {
    let src = '';

    if (icon) {
      const iconProperty = icon.properties.length > 0 ? icon.properties[0] : null;

      src += 'assets/icons/';
      src += icon.type.typeName + '/';
      src += icon.iconName + '/';
      src += icon.iconName;
      src += iconProperty.propertyPrefix ? '_' + iconProperty.propertyPrefix : '';
      src += iconProperty.propertySize;
      src += iconProperty.propertyFormat;
    }

    return src;
  }

  public getSidePanelIconSrc(icon) {
    let src = '';

    if (icon) {
      let iconProperty = icon.properties.length > 0 ? icon.properties[0] : null;

      if (icon.type.typeName === 'ming.le' || icon.type.typeName === 'application') {
        for (let i = 0; i < icon.properties.length; i++) {
          if (icon.properties[i].propertySize === '80x80') {
            iconProperty = icon.properties[i];
          }
        }
      }

      src += 'assets/icons/';
      src += icon.type.typeName + '/';
      src += icon.iconName + '/';
      src += icon.iconName;
      src += iconProperty.propertyPrefix ? '_' + iconProperty.propertyPrefix : '';
      src += iconProperty.propertySize;
      src += iconProperty.propertyFormat;
    }

    return src;
  }

  public getIconDownload(icon) {
    let dlName = '';

    if (icon) {
      dlName += icon.iconName;
      dlName += '.zip';
    }

    return dlName;
  }

  public getIconZip(icon) {
    let src = '';

    if (icon) {
      src += 'assets/icons/';
      src += icon.type.typeName + '/';
      src += icon.iconName + '/';
      src += icon.iconName;
      src += '.zip';
    }

    return src;
  }

  public getProductIcons(key) {
    const prodIcons: any[] = [];

    const len = this.IconMasterList.length;
    for (let i = 0; i < len; i++) {
      const icon = this.IconMasterList[i];

      const len2 = icon.products.length;
      for (let ii = 0; ii < len2; ii++) {
        const prod = icon.products[ii];

        if (prod.productName === key) {
          prodIcons.push(icon);
          break;
        }
      }
    }

    this.ProductIconList = prodIcons;

    return prodIcons;
  }

  public getRootLink() {
    return window.location.href;
  }

  public getSearchTags(icons, key) {
    const searchTags = [];

    const len = icons.length;
    for (let i = 0; i < len; i++) {
      const tags = icons[i].tags;

      const len2 = tags.length;
      for (let ii = 0; ii < len2; ii++) {
        const tag = tags[ii].tagName;
        let isExist = false;

        const len3 = searchTags.length;
        for (let iii = 0; iii < len3; iii++) {
          const searchTag = searchTags[iii];

          if (tag === searchTag) {
            isExist = true;
          }
        }

        if (!isExist && this.containsString(tag, key)) {
          searchTags.push(tag);
        }
      }
    }

    return searchTags;
  }

  public getSelectedIcons() {
    return SohoEngineComponent.SelectedIconList;
  }

  public unselectIcon(id) {
    const len = this.IconMasterList.length;
    for (let i = 0; i < len; i++) {
      const icon = this.IconMasterList[i];

      if (icon.iconId === id) {
        icon.isSelected = false;
        icon.visible = false;
        break;
      }
    }
  }

  public updateIconCount(icons) {
    const requestString = this.buildRequestString('iconUpdate', icons, null, null, null);

    const service = this._http.get(SohoEngineComponent.serverAddress + '/getIconUpdate?' + requestString)
        .map((res: Response) => res.json())
        .subscribe(data => {
          /* TODO: UPDATE CURRENT ARRAY */
          const x = data;
        });

    return service;
  }

  public updateProductIcon(icons, prodId, isNewProd, prodNew) {
    const requestString = this.buildRequestString('prodUpdate', icons, prodId, isNewProd, prodNew);

    const service = this._http.get(SohoEngineComponent.serverAddress + '/updateIconProductTransaction?' + requestString)
        .map((res: Response) => res.json())
        .subscribe(data => {
          /* TODO: UPDATE CURRENT ARRAY */
          const x = data;
        });

    return service;
  }

  public getDownloadUpdate(icons, emailAddress, prodId, isNewProd, prodNew) {
    const requestString = this.buildDownloadString(icons, emailAddress, prodId, isNewProd, prodNew);

    const service = this._http.get(SohoEngineComponent.serverAddress + '/getDownloadUpdate?' + requestString);

    return service;
  }

  public openNewTab(url) {
    window.open(url);
  }

  public showModal() {
    const self = this;
    const onDownloadClick = function(e, modal) {
      if (SohoEngineComponent.downloadIcon) {
        self.prodEmail = $('#mpo-email-address').val();
        self.prodId = self.cachedProdId || $('#product-dd').val() || '';
        self.isNewProduct = $('#cb-newprod:checkbox:checked').length > 0;
        self.prodNewName = $('#mpo-product-input').val();

        let productExist = false;
        for (let i = 0; i < self.ProductMasterList.length; i++) {
          let checker = self.ProductMasterList[i];
          if (checker.productName === self.prodNewName) {
            productExist = true;
            break;
          }
        }

        if (productExist) {
          // SHOW MODAL PRODUCT EXISTING AND DON'T DOWNLOAD
          const setModalError = function (opt) {
            opt = $.extend({
              buttons: [{
                text: 'OK',
                click: function(e, modal) {
                  modal.close();
                }
              }]
            }, opt);

            $('#modal-validation').modal(opt);
          };

          setModalError({
            'title': 'Warning!',
            'isAlert': true,
            'content': $('#modal-validation')
          });
        } else {
          const svc = self.getDownloadUpdate(self.IconsToDownload, self.prodEmail, self.prodId, self.isNewProduct, self.prodNewName);
          svc.map((res: Response) => res.json())
          .subscribe(data => {
            if (self.DownloadType === 'icon-view') {
              const sidebarDLLink = document.getElementById('sidebar-download-link');
              const rootLink = self.getRootLink();

              sidebarDLLink.setAttribute('href', rootLink + self.getIconZip(self.IconsToDownload[0]));
              sidebarDLLink.setAttribute('download', self.getIconDownload(self.IconsToDownload[0]));

              sidebarDLLink.click();
            } else {
              SohoEngineComponent.downloadIcon(data);
              DropPanelComponent.clearAll();
            }

            self.cachedProdId = self.prodId;

            // IconViewComponent.refreshData(true);
          });

          modal.close();
        }
      }
    };

    const setModal = function (opt) {
      opt = $.extend({
        buttons: [{
          text: 'Cancel',
          click: function(e, modal) {
            modal.close();
          }
        }, {
          text: 'Download',
          click: onDownloadClick,
          validate: false,
          isDefault: true
        }]
      }, opt);

      $('#modal-product-owner').modal(opt);
    };

    setModal({
      'title': 'Download',
      'content': $('#modal-product-owner')
    });

    this.checkModalFields();
  }

  public toggleNewProduct() {
    this.isNewProduct = !this.isNewProduct;
  }

  /* Private functions */
  private buildDownloadString(icons, emailAddress, prodId, isNewProd, prodNew) {
    let dString = '';

    const len = icons.length;
    for (let i = 0; i < len; i++) {
      const icon = icons[i];

      if (dString === '') {
        dString += 'data=' + 'emailAddress:' + emailAddress + ';productId:' + prodId + ';isNewProduct:'
        + isNewProd + ';productNew:' + prodNew + ';iconList:';
      }

      dString += icon.iconId + '†' + icon.iconCounter + '§';
    }

    return dString;
  }

  private buildRequestString(type, icons, prodId, isNewProd, prodNew) {
    let dString = '';

    const len = icons.length;
    for (let i = 0; i < len; i++) {
      const icon = icons[i];

      if (type === 'iconUpdate') {
        dString +=  'iconId:' + icon.iconId + ';iconCounter:' + icon.iconCounter + ';¶';
      } else if (type === 'prodUpdate') {
        if (dString === '') {
          dString += 'productId:' + prodId + ';isNewProduct:' + isNewProd + ';productNew:' + prodNew + ';iconList:';
        }

        dString += icon.iconId + '§';
      }
    }

    return dString;
  }

  private containsString(main, key) {
    main = main.toUpperCase();
    key = key.toUpperCase();

    return main.indexOf(key) !== -1;
  }

  private containsProduct(icon, key) {
    const len = icon.products.length;
    for (let i = 0; i < len; i++) {
      const prod = icon.products[i];

      if (this.containsString(prod.productName, key)) {
        return true;
      }
    }

    return false;
  }

  private containsTag(icon, key) {
    const len = icon.tags.length;
    for (let i = 0; i < len; i++) {
      const tag = icon.tags[i];

      if (this.containsString(tag.tagName, key)) {
        return true;
      }
    }

    return false;
  }

  private getIcons() {
    const service = this._http.get(SohoEngineComponent.serverAddress + '/getIcons')
                  .map((res: Response) => res.json())
                  .subscribe(data => {
                    this.IconMasterList = data;
                    this.IconFilteredList = data;

                    if (this.afterGetIcons) {
                      this.afterGetIcons(this.IconFilteredList);
                    }

                    if (this.afterGetProducts) {
                      this.afterGetProducts(this.ProductMasterList);
                    }

                    this.getProducts();
                  });

    return service;
  }

  private getProducts() {
    const service = this._http.get(SohoEngineComponent.serverAddress + '/getProducts')
                  .map((res: Response) => res.json())
                  .subscribe(data => {
                    this.ProductMasterList = data;

                  });

    return service;
  }

  private getTypes() {
    const service = this._http.get(SohoEngineComponent.serverAddress + '/getTypes')
                  .map((res: Response) => res.json())
                  .subscribe(data => {
                    this.IconTypeList = data;

                    if (this.afterGetTypes) {
                      this.afterGetTypes(this.IconTypeList);
                    }

                    this.getIcons();
                  });

    return service;
  }

}
