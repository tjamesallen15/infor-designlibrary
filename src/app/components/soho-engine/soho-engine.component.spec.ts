import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SohoEngineComponent } from './soho-engine.component';

describe('SohoEngineComponent', () => {
  let component: SohoEngineComponent;
  let fixture: ComponentFixture<SohoEngineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SohoEngineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SohoEngineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
