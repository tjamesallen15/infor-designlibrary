import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-persona-view',
  templateUrl: './persona-view.component.html',
  styleUrls: ['./persona-view.component.css']
})
export class PersonaViewComponent implements OnInit {
  public under_construction = 'assets/under-construction.png';

  constructor() { }

  ngOnInit() {
  }

}
