import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-code-view',
  templateUrl: './code-view.component.html',
  styleUrls: ['./code-view.component.css']
})
export class CodeViewComponent implements OnInit {
  public under_construction = 'assets/under-construction.png';
  
  constructor() { }

  ngOnInit() {
  }

}
