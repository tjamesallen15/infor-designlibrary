import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IconViewComponent } from './../components/icons/icon-view/icon-view.component';
import { AdminIconComponent } from './../components/admin/admin-icon/admin-icon.component';
import { BrandViewComponent } from '../components/brand/brand-view/brand-view.component';
import { CodeViewComponent } from '../components/code/code-view/code-view.component';
import { PersonaViewComponent } from '../components/persona/persona-view/persona-view.component';

export const routes: Routes = [
  { path: '',                      redirectTo: 'hnl-dev/2.1.1/app-icon-view', pathMatch: 'full' }, // default
  { path: 'hnl-dev/2.1.1/app-icon-view',         component: IconViewComponent },
  { path: 'hnl-dev/2.1.1/app-admin-icon',        component: AdminIconComponent },
  { path: 'hnl-dev/2.1.1/app-brand-view',         component: BrandViewComponent },
  { path: 'hnl-dev/2.1.1/app-code-view',         component: CodeViewComponent },
  { path: 'hnl-dev/2.1.1/app-persona-view',         component: PersonaViewComponent }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes);
