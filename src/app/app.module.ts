import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './routes/app.routes';

import { SohoComponentsModule } from '@infor/sohoxi-angular';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/templates/header/header.component';
import { ApplicationMenuComponent } from './components/templates/application-menu/application-menu.component';
import { IconViewComponent } from './components/icons/icon-view/icon-view.component';
import { SohoEngineComponent } from './components/soho-engine/soho-engine.component';
import { DropPanelComponent } from './components/templates/drop-panel/drop-panel.component';
import { AdminIconComponent } from './components/admin/admin-icon/admin-icon.component';
import { BrandViewComponent } from './components/brand/brand-view/brand-view.component';
import { PersonaViewComponent } from './components/persona/persona-view/persona-view.component';
import { CodeViewComponent } from './components/code/code-view/code-view.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ApplicationMenuComponent,
    IconViewComponent,
    SohoEngineComponent,
    DropPanelComponent,
    AdminIconComponent,
    BrandViewComponent,
    PersonaViewComponent,
    CodeViewComponent
  ],
  imports: [
    HttpModule,
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    SohoComponentsModule
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
